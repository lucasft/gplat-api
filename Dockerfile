FROM node:9
WORKDIR /app
COPY . /app
RUN apt-get update && apt-get install -y build-essential && apt-get install -y python
RUN npm install
ENV DATABASE admin
ENV PORT 27017
ENV USERNAME root
ENV PASSWORD example
ENV HOST localhost
CMD [ "npm", "start"]
