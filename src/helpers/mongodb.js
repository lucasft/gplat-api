async function mongoPagination(collection, paramBusca, tipoOrdem, limite, pagina) {
  const retornoCollection = await collection.find(paramBusca)
    .skip((limite * pagina) - limite)
    .limit(limite)
    .sort({ paramSort: tipoOrdem })
  return retornoCollection
}

async function countDocuments(collection, param) {
  const countCollectionDocs = await collection.count(param)
  return countCollectionDocs
}

export { mongoPagination, countDocuments }
