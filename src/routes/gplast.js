import express from 'express'
const router = express.Router()
import { doCount } from '../controllers/gplast'

router.route("/doCount")
  .post(doCount)

export default router
