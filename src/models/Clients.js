import mongoose from 'mongoose'
const mongoose_delete = require('mongoose-delete')
const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectId

const clientSchema = new Schema({
  id: {
    type: ObjectId
  },
  clientCode: {
    type: Number
  },
  cnpj: {
    type: String,
    default: ""
  },
  name: {
    type: String,
    default: ""
  },
  directTopPosition: {
    type: Number,
  },
  jobTitle: {
    type: String
  },
  exception: {
    type: Boolean,
    default: false,
    required: true
  },
  address: {
    latitude: {
      type: String,
      default: ""
    },
    longitude: {
      type: String,
      default: ""
    },
    district: {
      type: String,
      default: ""
    },
    city: {
      type: String,
      default: ""
    },
    zipCode: {
      type: String,
      default: ""
    },
    fu: {
      type: String,
      default: ""
    },
  }
}, { timestamps: { createdAt: 'created_at' } })

clientSchema.index({ clientCode: 1 }, { unique: true })
clientSchema.plugin(mongoose_delete, { overrideMethods: true });

clientSchema.post('save', function (err, doc, next) {
  if (err) {
    next(err);
  }
  else {
    next();
  }
});

clientSchema.set('collection', 'Clients')
const Clients = mongoose.model('Clients', clientSchema);

export default Clients
