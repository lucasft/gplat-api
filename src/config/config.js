import dotenv from 'dotenv'
dotenv.config()

const BD = {
  banco: process.env.DB_NAME,
  usuario: process.env.DB_USERNAME,
  senha: process.env.DB_PASSWORD,
  host: process.env.DB_HOSTNAME,
  porta: process.env.DB_PORT
}

export default BD
