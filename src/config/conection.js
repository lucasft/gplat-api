import mongoose from 'mongoose'
import DB from './config'

const uri = `mongodb://${DB.usuario}:${DB.senha}@${DB.host}:${DB.porta}/${DB.banco}`;
// const uri = `mongodb://localhost/StudentId`
mongoose.Promise = global.Promise;
mongoose.connect(uri, { useNewUrlParser: true }, (error) => {
    if (error) {
        console.log(error);
    }
});
