-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema jbs
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema jbs
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `jbs` DEFAULT CHARACTER SET utf8 ;
-- -----------------------------------------------------
-- Schema jbs
-- -----------------------------------------------------
USE `jbs` ;

-- -----------------------------------------------------
-- Table `jbs`.`Users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jbs`.`Users` (
  `idUsers` INT NOT NULL,
  `login` VARCHAR(45) NULL,
  `password` VARCHAR(45) NULL,
  PRIMARY KEY (`idUsers`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jbs`.`Employees`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jbs`.`Employees` (
  `idEmployees` INT NOT NULL,
  `directTopPosition` INT NULL,
  `jobTitle` VARCHAR(45) NULL,
  `available` TINYINT(1) NULL,
  `noviceSalesman` TINYINT(1) NULL,
  `localization` VARCHAR(45) NULL,
  `idUsers` INT NULL,
  PRIMARY KEY (`idEmployees`, `directTopPosition`, `idUsers`),
  INDEX `fk_Employees_1_idx` (`directTopPosition` ASC),
  INDEX `fk_Employees_Users1_idx` (`idUsers` ASC),
  CONSTRAINT `fk_Employees_1`
    FOREIGN KEY (`directTopPosition`)
    REFERENCES `jbs`.`Employees` (`idEmployees`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Employees_Users1`
    FOREIGN KEY (`idUsers`)
    REFERENCES `jbs`.`Users` (`idUsers`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jbs`.`Routes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jbs`.`Routes` (
  `idRoutes` INT NOT NULL,
  `date` DATE NOT NULL,
  `idSalesman` INT NOT NULL,
  `idCoach` INT NOT NULL,
  PRIMARY KEY (`idRoutes`, `date`, `idSalesman`, `idCoach`),
  INDEX `fk_Routes_Employees1_idx` (`idSalesman` ASC, `idCoach` ASC),
  CONSTRAINT `fk_Routes_Employees1`
    FOREIGN KEY (`idSalesman` , `idCoach`)
    REFERENCES `jbs`.`Employees` (`idEmployees` , `directTopPosition`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jbs`.`Coachings`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jbs`.`Coachings` (
  `idCoachings` INT NOT NULL,
  `date` DATE NULL,
  `qntdValidPDV` VARCHAR(45) NULL,
  `idSalesman` INT NOT NULL,
  `idCoach` INT NOT NULL,
  `idRoutes` INT NOT NULL,
  `routesDate` DATE NOT NULL,
  PRIMARY KEY (`idCoachings`, `idSalesman`, `idCoach`, `idRoutes`, `routesDate`),
  INDEX `fk_Coachings_Employees2_idx` (`idSalesman` ASC, `idCoach` ASC),
  INDEX `fk_Coachings_Routes1_idx` (`idRoutes` ASC, `routesDate` ASC),
  CONSTRAINT `fk_Coachings_Employees2`
    FOREIGN KEY (`idSalesman` , `idCoach`)
    REFERENCES `jbs`.`Employees` (`idEmployees` , `idEmployees`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Coachings_Routes1`
    FOREIGN KEY (`idRoutes` , `routesDate`)
    REFERENCES `jbs`.`Routes` (`idRoutes` , `date`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jbs`.`PDVs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jbs`.`PDVs` (
  `idPDVs` INT NOT NULL,
  `status` VARCHAR(45) NULL,
  `localization` VARCHAR(45) NULL,
  `visitedDate` DATE NULL,
  `type` ENUM('DEFAULT', 'EXCEPTION') NULL,
  `idEmployees` INT NULL,
  PRIMARY KEY (`idPDVs`, `idEmployees`),
  INDEX `fk_PDVs_Employees1_idx` (`idEmployees` ASC),
  CONSTRAINT `fk_PDVs_Employees1`
    FOREIGN KEY (`idEmployees`)
    REFERENCES `jbs`.`Employees` (`idEmployees`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jbs`.`Forms`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jbs`.`Forms` (
  `idForms` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `description` VARCHAR(45) NULL,
  `process` VARCHAR(45) NULL,
  `segment` VARCHAR(45) NULL,
  `status` TINYINT(1) NULL,
  `dateRegister` DATE NULL,
  `jobsTitle` VARCHAR(45) NULL,
  PRIMARY KEY (`idForms`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jbs`.`Questions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jbs`.`Questions` (
  `idQuestions` INT NOT NULL,
  `type` VARCHAR(45) NULL,
  `text` VARCHAR(45) NULL,
  `pass` VARCHAR(45) NULL,
  `stage` VARCHAR(45) NULL,
  `status` VARCHAR(45) NULL,
  `cancelPDV` TINYINT(1) NULL,
  `idForms` INT NOT NULL,
  PRIMARY KEY (`idQuestions`, `idForms`),
  INDEX `fk_Questions_Forms1_idx` (`idForms` ASC),
  CONSTRAINT `fk_Questions_Forms1`
    FOREIGN KEY (`idForms`)
    REFERENCES `jbs`.`Forms` (`idForms`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jbs`.`Options`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jbs`.`Options` (
  `idOptions` INT NOT NULL,
  `type` VARCHAR(45) NULL,
  `description` VARCHAR(45) NULL,
  `value` VARCHAR(45) NULL,
  `status` TINYINT(1) NULL,
  `idQuestions` INT NOT NULL,
  PRIMARY KEY (`idOptions`, `idQuestions`),
  INDEX `fk_Answers_Questions1_idx` (`idQuestions` ASC),
  CONSTRAINT `fk_Answers_Questions1`
    FOREIGN KEY (`idQuestions`)
    REFERENCES `jbs`.`Questions` (`idQuestions`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jbs`.`Routes_has_PDVs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jbs`.`Routes_has_PDVs` (
  `idRoutes` INT NOT NULL,
  `idPDVs` INT NOT NULL,
  PRIMARY KEY (`idRoutes`, `idPDVs`),
  INDEX `fk_Routes_has_PDVs_PDVs1_idx` (`idPDVs` ASC),
  INDEX `fk_Routes_has_PDVs_Routes1_idx` (`idRoutes` ASC),
  CONSTRAINT `fk_Routes_has_PDVs_Routes1`
    FOREIGN KEY (`idRoutes`)
    REFERENCES `jbs`.`Routes` (`idRoutes`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Routes_has_PDVs_PDVs1`
    FOREIGN KEY (`idPDVs`)
    REFERENCES `jbs`.`PDVs` (`idPDVs`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jbs`.`InconsistenceLocalization`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jbs`.`InconsistenceLocalization` (
  `idInconsistenceLocalization` INT NOT NULL,
  PRIMARY KEY (`idInconsistenceLocalization`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jbs`.`Check`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jbs`.`Check` (
  `idCheck` INT NOT NULL,
  `date` DATE NULL,
  `hour` VARCHAR(45) NULL,
  `localization` VARCHAR(45) NULL,
  `type` ENUM('in', 'out') NULL,
  `idCoachings` INT NOT NULL,
  `status` ENUM('APROVED', 'WAITING', 'REFUSED') NULL,
  `idPDVs` INT NOT NULL,
  `idInconsistenceLocalization` INT NULL,
  PRIMARY KEY (`idCheck`, `idCoachings`, `idPDVs`, `idInconsistenceLocalization`),
  INDEX `fk_Check_Coachings1_idx` (`idCoachings` ASC),
  INDEX `fk_Check_PDVs1_idx` (`idPDVs` ASC),
  INDEX `fk_Check_InconsistenceLocalization1_idx` (`idInconsistenceLocalization` ASC),
  CONSTRAINT `fk_Check_Coachings1`
    FOREIGN KEY (`idCoachings`)
    REFERENCES `jbs`.`Coachings` (`idCoachings`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Check_PDVs1`
    FOREIGN KEY (`idPDVs`)
    REFERENCES `jbs`.`PDVs` (`idPDVs`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Check_InconsistenceLocalization1`
    FOREIGN KEY (`idInconsistenceLocalization`)
    REFERENCES `jbs`.`InconsistenceLocalization` (`idInconsistenceLocalization`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jbs`.`CouchingConfig`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jbs`.`CouchingConfig` (
  `idCouchingConfig` INT NOT NULL,
  `maxPDVOffRoad` INT NULL,
  `minPDVSuccessCoaching` INT NULL,
  `toleranceRadius` VARCHAR(45) NULL,
  PRIMARY KEY (`idCouchingConfig`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jbs`.`CoachingForm`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jbs`.`CoachingForm` (
  `idCoachingForm` INT NOT NULL,
  `status` ENUM('FINISH', 'WAITING', 'CORRECTED') NULL,
  `score` VARCHAR(45) NULL,
  `type` ENUM('PRE', 'DURING', 'POS') NULL,
  `idCoachings` INT NOT NULL,
  `idForms` INT NOT NULL,
  PRIMARY KEY (`idCoachingForm`, `idCoachings`, `idForms`),
  INDEX `fk_CoachingForm_Coachings1_idx` (`idCoachings` ASC),
  INDEX `fk_CoachingForm_Forms1_idx` (`idForms` ASC),
  CONSTRAINT `fk_CoachingForm_Coachings1`
    FOREIGN KEY (`idCoachings`)
    REFERENCES `jbs`.`Coachings` (`idCoachings`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CoachingForm_Forms1`
    FOREIGN KEY (`idForms`)
    REFERENCES `jbs`.`Forms` (`idForms`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jbs`.`AnswersCoachingForm`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jbs`.`AnswersCoachingForm` (
  `idAnswersCoachingForm` INT NOT NULL,
  `type` VARCHAR(45) NULL,
  `answer` VARCHAR(45) NULL,
  `status` ENUM('CORRECT', 'INCORRECT', 'WAITING') NULL,
  `idCoachingForm` INT NOT NULL,
  PRIMARY KEY (`idAnswersCoachingForm`, `idCoachingForm`),
  INDEX `fk_AnswersCoachingForm_CoachingForm1_idx` (`idCoachingForm` ASC),
  CONSTRAINT `fk_AnswersCoachingForm_CoachingForm1`
    FOREIGN KEY (`idCoachingForm`)
    REFERENCES `jbs`.`CoachingForm` (`idCoachingForm`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jbs`.`GoalCoaching`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jbs`.`GoalCoaching` (
  `idGoalCoaching` INT NOT NULL,
  `goal` INT NULL,
  `startDate` DATE NULL,
  `endDate` DATE NULL,
  `idEmployees` INT NOT NULL,
  PRIMARY KEY (`idGoalCoaching`, `idEmployees`),
  INDEX `fk_GoalCoaching_Employees1_idx` (`idEmployees` ASC),
  CONSTRAINT `fk_GoalCoaching_Employees1`
    FOREIGN KEY (`idEmployees`)
    REFERENCES `jbs`.`Employees` (`idEmployees`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jbs`.`PontuationHistory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jbs`.`PontuationHistory` (
  `idPontuationHistory` INT NOT NULL,
  `date` DATETIME NULL,
  `score` VARCHAR(45) NULL,
  `idEmployees` INT NOT NULL,
  `idCoachings` INT NOT NULL,
  PRIMARY KEY (`idPontuationHistory`, `idEmployees`, `idCoachings`),
  INDEX `fk_PontuationHistory_Employees1_idx` (`idEmployees` ASC),
  INDEX `fk_PontuationHistory_Coachings1_idx` (`idCoachings` ASC),
  CONSTRAINT `fk_PontuationHistory_Employees1`
    FOREIGN KEY (`idEmployees`)
    REFERENCES `jbs`.`Employees` (`idEmployees`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PontuationHistory_Coachings1`
    FOREIGN KEY (`idCoachings`)
    REFERENCES `jbs`.`Coachings` (`idCoachings`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jbs`.`Hierarchies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jbs`.`Hierarchies` (
  `idHierarchies` INT NOT NULL,
  `jsonHierarchy` LONGTEXT NULL,
  `startDate` DATE NULL,
  `endDate` DATE NULL,
  `idEmployees` INT NOT NULL,
  PRIMARY KEY (`idHierarchies`, `idEmployees`),
  INDEX `fk_TooltipHierarchy_Employees1_idx` (`idEmployees` ASC),
  CONSTRAINT `fk_TooltipHierarchy_Employees1`
    FOREIGN KEY (`idEmployees`)
    REFERENCES `jbs`.`Employees` (`idEmployees`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jbs`.`SuportTicket`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jbs`.`SuportTicket` (
  `idSuportTicket` INT NOT NULL,
  `message` TEXT(250) NULL,
  `idEmployees` INT NOT NULL,
  PRIMARY KEY (`idSuportTicket`, `idEmployees`),
  INDEX `fk_SuportTicket_Employees1_idx` (`idEmployees` ASC),
  CONSTRAINT `fk_SuportTicket_Employees1`
    FOREIGN KEY (`idEmployees`)
    REFERENCES `jbs`.`Employees` (`idEmployees`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
